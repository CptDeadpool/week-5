﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {

	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;
	public Transform target1;
	public Transform target2;
	public Transform currentTarget;

	// private state
	private float speed;
	private float turnSpeed;

	//private Transform target;
	private Vector2 heading;

	void Start() {
		// find the player to set the target

		PlayerMove p = FindObjectOfType<PlayerMove>();
		target1 = p.transform;

		// bee initially moves in random direction
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate(angle);

		// set speed and turnSpeed randomly 
		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, 
			Random.value);
	}
		
	void Update() {
		// get the vector from the bee to the target 
		Vector2 direction1 = target1.position - transform.position;
		/*Vector2 direction2 = target2.position - transform.position;

		if (direction1.magnitude < direction2.magnitude) {
			currentTarget = target1;
		} else {
			currentTarget = target2;
		}
		Vector2 direction = currentTarget.position - transform.position;
		// calculate how much to turn per frame*/
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction1.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}

		transform.Translate(heading * speed * Time.deltaTime);
	}

}
