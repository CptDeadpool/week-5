﻿using UnityEngine;
using System.Collections;

public class BeeSpawner : MonoBehaviour {
	public Transform currentTarget;


	public BeeMove beePrefab;
	public PlayerMove player;
	public PlayerMove player2;
	public int nBees = 50; 

	public float xMin, yMin;
	public float width, height;
	private float timeTillNext,TimeSinceLast = 0f;
	public float minBeeP, MaxBeeP;
	private int beeNum = 0;
	// Use this for initialization
	void Start () {
		createBee ();

		/*for (int i = 0; i < nBees; i++) {
			BeeMove bee = Instantiate (beePrefab);

			bee.transform.parent = transform;
			bee.gameObject.name = "Bee " + i;

			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);

		}*/

	}
	public void DestroyBees(Vector2 center, float radius){
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild (i);
			Vector2 v = (Vector2)child.position - center;
			if (v.magnitude <= radius) {
				Destroy (child.gameObject);
			}

		}

	}
	void createBee(){
		BeeMove bee = Instantiate (beePrefab);

		bee.transform.parent = transform;
		bee.gameObject.name = "Bee " + beeNum;

		float x = xMin + Random.value * width;
		float y = yMin + Random.value * height;
		bee.transform.position = new Vector2 (x, y);

		timeTillNext =  Mathf.Lerp (minBeeP, MaxBeeP, Random.value);
		beeNum++;
		TimeSinceLast = 0f;
		Debug.Log ("HI");
	}
	// Update is called once per frame
	void Update () {
		TimeSinceLast += Time.deltaTime;
		if (TimeSinceLast >= timeTillNext) {
			createBee ();
		}
	}
}