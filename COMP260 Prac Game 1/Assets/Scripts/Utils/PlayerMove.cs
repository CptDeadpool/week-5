﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {
	
	public float destroyRadius = 1.0f; 
	public float maxSpeed = 5.0f;
	private BeeSpawner beeSpawner;
	public bool player1 = true;

	void Start() {
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}
	

	void Update() {
		if (player1) {
			if (Input.GetButton ("Fire1")) {
				// destroy nearby bees
				beeSpawner.DestroyBees (
					transform.position, destroyRadius);
			}
		}else {
			if (Input.GetButton ("Fire2")) {
				// destroy nearby bees
				beeSpawner.DestroyBees (
					transform.position, destroyRadius);
			}
		}



		// get the input values
		if (player1) {
			Vector2 direction;
			direction.x = Input.GetAxis ("Horizontal");
			direction.y = Input.GetAxis ("Vertical");

			// scale by the maxSpeed parameter
			Vector2 velocity = direction * maxSpeed;

			// move the object
			transform.Translate (velocity * Time.deltaTime);
		} else {
			Vector2 direction2;
			direction2.x = Input.GetAxis ("Horizontal 2");
			direction2.y = Input.GetAxis ("Vertical 2");

			// scale by the maxSpeed parameter
			Vector2 velocity = direction2 * maxSpeed;

			// move the object
			transform.Translate (velocity * Time.deltaTime);
	}
}

}
